*** Settings ***
Documentation  This Test case will validates different combinations of
...            email and passwords and check the login form of
...            Amazon.sg website through Data Driven Testing in Test Case level
Library     SeleniumLibrary
Resource  ../Resources/AmazonKeywords.robot
Resource  ../Resources/AmazonVariables.robot
Resource  ../Resources/AmazonVariables.robot
Resource  ../Resources/AmazonLoginResoure.robot


Suite Setup     open the browser
Suite Teardown  close the browser
Test Template    Invalid Login

# usernames and pwds are tried on testcase level
*** Test Cases ***      username      Passwd
TC1:Right email and empty pwd     lekshmitesting1@gmail.com   {Empty}
TC1:Right email and wrong pwd      lekshmitesting1@gmail.com   23456
TC1:Wrong email and empty pwd       123@gmail.com            {Empty}
TC1:Wrong email and wrong pwd       123@gmail.com             23456
TC1:Empty email and empty pwd       {Empty}                  {Empty}

*** Keywords ***
Invalid Login
  Go to the sign in page
  [Arguments]  ${username}   ${Passwd}
  Input username   ${username}
  Input pwd  ${Passwd}
  Click Login
  Validate And Print Error Message
