*** Settings ***
Documentation  This Test case will validates different combinations of
...            email and passwords and check the login form of
...            Amazon.sg website through Data Driven Testing.
...            Test data will be taken for the test data excel file.

Library     SeleniumLibrary
Resource  ../Resources/AmazonKeywords.robot
Resource  ../Resources/AmazonVariables.robot
Resource  ../Resources/AmazonVariables.robot
Resource  ../Resources/AmazonLoginResoure.robot
Library   DataDriver   ../Test Data/SigninData1.xlsx  sheet_name=Sheet1


Suite Setup     open the browser
Suite Teardown  close the browser
Test Template    Invalid Login


*** Test Cases ***
Signin Test Validations with Excel   ${username}    ${Passwd}


*** Keywords ***
Invalid Login
  Go to the sign in page
  [Arguments]  ${username}   ${Passwd}
  Input username   ${username}
  Input pwd  ${Passwd}
  Click Login
  Validate And Print Error Message
