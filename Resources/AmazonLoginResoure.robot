*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/AmazonKeywords.robot
Resource  ../Resources/AmazonVariables.robot

*** Keywords ***
open the browser
  Open Amazon.sg Website   ${url}     ${Browser}

Go to the sign in page
  go to     https://www.amazon.sg/ap/signin?_encoding=UTF8&ignoreAuthState=1&openid.assoc_handle=sgflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.sg%2F%3Fref_%3Dnav_custrec_signin&switch_account=
  sleep  2

Input username
  [Arguments]     ${username}
  input text      ${EmailFeild}   ${username}
  click button    ${ContinueBtn}

Input pwd
  [Arguments]     ${Passwd}
  input password  ${PasswordFeild}  ${Passwd}
  sleep  2

Click Login
  click button    ${SubmitBtn}

Validate And Print Error Message
   click element  class:a-list-item
   ${error_message} =  Get Text   class:a-list-item
   log to console  ${error_message}

close the browser
  close all browsers






