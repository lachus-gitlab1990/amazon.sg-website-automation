*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${url}               https://www.amazon.sg/
${Browser}           Chrome
${SignIn}            css:[data-nav-role='signin']
${EmailFeild}        id:ap_email
${Email}             lekshmitesting1@gmail.com
${ContinueBtn}       id:continue
${PasswordFeild}     id=ap_password
${Password}          testaccount
${SubmitBtn}         id=signInSubmit
${Cart}              css:[aria-label='0 items in cart']
${EmptyCartMsgId}    css:[data-name='Active Cart']
${EmptycartMessage}  Your Shopping Cart is empty
#${}