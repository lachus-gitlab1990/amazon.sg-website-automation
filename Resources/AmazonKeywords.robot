*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/AmazonVariables.robot

*** Keywords ***
Open Amazon.sg Website
  [Arguments]     ${WebUrl}     ${WebBrowser}
  OPEN BROWSER    ${WebUrl}     ${WebBrowser}
  sleep  2
  ${title}       get title
  log to console  The title of the page is ${title}
Sign-in with Account Credentials
  click link      ${SignIn}
  sleep  2
  input text      ${EmailFeild}    ${Email}
  click button    ${ContinueBtn}
  input password  ${PasswordFeild}  ${Password}
  click button    ${SubmitBtn}
Check Cart is empty
   click link     ${Cart}
   element should contain  ${EmptyCartMsgId}   ${EmptycartMessage}

Count and extract the number of links in the Home page
    ${AllLinkCount} =  get element count  xpath://a
    log to console  ${AllLinkCount}

    @{LinkItems} =  create list
     : FOR  ${i}  IN RANGE  1  ${AllLinkCount}
     ${LinkText}=  get text  xpath:(//a)[${i}]
     log to console  ${LinkText}
     END
